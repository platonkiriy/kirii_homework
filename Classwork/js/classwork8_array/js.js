/* TASK - 1
 * Create a function that does not receive any arguments
 * it will only ask the user what dishes he prefers to eat on breakfast.
 * User can enter only one dish simultaneously, so you need to ask him UNTIL he will enter 'end'.
 * Each dish needs to be placed into an array.
 * Return value: array with all of the dishes.
 * */

// function getBreakfast() {
//     const breakfastDishes = [];
//     let userInput = prompt('what dishes you prefer to eat on breakfast')
//
//     while (!userInput.includes('end')) {
//         breakfastDishes.push(userInput);
//         userInput = prompt('what dishes you prefer to eat on breakfast')
//     }
//     return breakfastDishes
// }
//
// console.log(getBreakfast())


/* TASK - 2
 * Create a function that will take one argument - array from the previous task result.
 * The task is to show every dish from the array in the console and delete it from the source array.
 * After all of the dishes will be shown on the console, the array must be empty.
 * */

// const morningBreakfast = getBreakfast()
// function cleanArray(array) {
//     const length = array.length;
//     for (let i = 0; i < length; i++) {
//         console.log(array.shift())
//
//     }
//     console.log(array)
// }
//
// cleanArray(morningBreakfast);


/* TASK - 3
 * Create a function that will receive an array as a single argument.
 * return value: new array, which is the exact copy of the source one.
 * Task needs to be done using: for, map(), spread operator. It means three implementations.
 */

const array = ['sss', 'ddd', 'fff', 'ggg']

// with for loop
// function copyArray(initialArray) {
//     const result = []
//     for (let i = 0; i < initialArray.length; i++) {
//         result.push(initialArray[i])
//     }
//     return result
// }

// with map() example1
// function copyArray(initialArray) {
//     const result = initialArray.map((element) => element)
//     return result
// }
// with map() example2
// const copyArray = initialArray => initialArray.map((element) => element)

// with spread operator example1
// function copyArray(initialArray) {
//     return [...initialArray]
// }

// with spread operator example2
const copyArray = initialArray => [...initialArray]


console.log(copyArray(array))