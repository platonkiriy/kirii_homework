/* ЗАДАНИЕ - 2
 * Написать функцию которая будет принимать два аргумента - число с которого начать отсчет и число до которого нужно досчитать.
 * Под отсчетом имеется в виду последовательный вывод чисел в консоль с увеличением на единицу.
 */
// function showNumbers(startNumber, endNumber) {
//     if (startNumber < endNumber)
//         for (let i = startNumber; i <= endNumber; i++) {
//             console.log(i)
//         } else {
//         for (let i = startNumber; i >= endNumber; i--) {
//             console.log(i)
//         }
//     }
// }
//
// showNumbers(5, 3);


/* ЗАДАНИЕ - 3
 * Написать функцию, которая будет суммировать ВСЕ числа, которые будут переданы ей в качестве аргументов.
 * */

// function summAllArgs() {
//     let summ = 0;
//     if (arguments.length === 0) {
//         return null
//     }
//
//     for (let i = 0; i < arguments.length; i++) {
//         summ += arguments[i];
//     }
//     return summ
// }
//
// let total = summAllArgs(12,312,345,452,235,434,)
//
// document.write(total)

/* ЗАДАНИЕ - 4
 * Написать функцию, которая из всех переданных аргументов возвращает только выбранный тип данных.
 * Аргументы:
 *   1 - выбранный тип данных, который нужно отоборать
 *   2 и далее - елементы, из которых нужно отобрать
 *
 * ДОП. ЗАДАНИЕ: Написать вторую реализацию, где элементы из которых нужно отбирать переданы в массиве.
 * */

// function filterDataType(dataType, ...otherArgs){
//     const filterResult = [];
//     for (let i = 0; i < otherArgs.length; i++) {
//         if(typeof otherArgs[i] === dataType) {
//             filterResult.push(otherArgs[i])
//         }
//     }
//     return filterResult;
// }
//
//
// let res = filterDataType('string', [3,2,1], {}, 234, 'text', [1,2,3], 'www')
// console.log(res)


/* ЗАДАНИЕ - 5
* Написать функцию getMultipleOf, которая будет выводить в консоль ВСЕ числа в заданом диапазоне, которые кратны третьему аргументу.
* Аргументы функции:
*   1) число С которого начинается диапазон (включительно)
*   2) число которым диапазон заканчивается (включительно)
*   3) число КРАТНЫЕ КОТОРОМУ нужно вывести в консоль
* Возвращаемое значение - отсутствует
* */

function getMultipleOf(start, end, devide) {
    if (devide === 0) return null

    if (start > end) {
        let temp = start
        start = end
        end = temp
    }
    for (let i = start; i < end; i++) {
        if (i % devide === 0) {
            document.write(`<li>${i}</li>`)
        }
    }
}

getMultipleOf(13, 115, 10)