/* TASK - 1
 * Create a square.
 * Ask the user about the size and background color of the square.
 * Create an element in JS
 * Ask the size.
 * Ask the background color.
 * Add the styles to the element in JS
 * Place the element BEFORE the first script tag on your page
 */

// example 1
// let sizeUser = prompt('Введите размер квадрата')
// let colorUser = prompt('Введите цвет квадрата')
// let square = document.createElement('div')

// square.style.backgroundColor = color
// square.style.width = size + 'px'
// square.style.height = size + 'px'


// let script = document.querySelector('script')
// script.before(square)


// example 2
// let sizeUser = prompt('Введите размер квадрата')
// let colorUser = prompt('Введите цвет квадрата')

// function createSquare(size, color, parent){
//     let square = document.createElement('div')
//     square.style.cssText = `width:${size}px; height:${size}px; background-color:${color}`
//     parent.before(square)
// }

// let script = document.querySelector('script')
// createSquare(sizeUser, colorUser, script)

// example 3
// function createSquare(size, color){
//     let square = document.createElement('div')
//     square.style.cssText = `width:${size}px; height:${size}px; background-color:${color}`
//     return square
// }

// let script = document.querySelector('script')

// const someBlock = createSquare(50, 'coral')


// script.before(someBlock)
// someBlock.style.backgroundColor = 'blue'
// console.log(someBlock);

/* TASK - 2
 * Create two squares, using the same way that described in the previous task.
 * But these squares are going to have different background colors.
 * Create two elements in JS
 * Ask the user about the size for both of these squares
 * Ask the user about the fist background-color
 * Ask the user about the second background-color
 * Add the styles to both squares
 * Place both squares BEFORE the first element with the script tag on the page
 */

// let userSize = prompt('enter size')
// let userFirstColor = prompt('enter color first')
// let userSecondColor = prompt('enter color second')
// let firstSquare = document.createElement('div')
// let secondSquare = document.createElement('div')
//
// firstSquare.style.cssText = `background-color:${userFirstColor}; width:${userSize}px; height:${userSize}px`
// secondSquare.style.cssText = `background-color:${userSecondColor}; width:${userSize}px; height:${userSize}px`
//
// let script = document.querySelector('script')
// script.before(firstSquare)
// script.before(secondSquare)
//
// const makeComponent = (tag, size = 'max-content', color = 'inherit') => {
//     if (!tag) return null
//     const element = document.createElement(tag)
//
//     element.style.cssText = `background-color:${color}; width:${size}px; height:${size}px`
//     return element
// }
//
// function createSomeSquare(count, callback) {
//     let script = document.querySelector('script')
//     for (let i = 0; i < count; i++) {
//         let size = prompt('enter size')
//         let color = prompt('enter color')
//
//         const square = callback('div', size, color)
//         script.before(square)
//     }
// }
//
// createSomeSquare(2, makeComponent)


/*написать функционал для создания прямоугольников, количество штук должен указать пользователь*/

function createNumberBlocks(number) {
    for (let i = 0; i < number; i++) {
        let script = document.querySelector('script')
        const block = document.createElement('div')
        block.className = 'block'
        script.before(block)
    }
}

createNumberBlocks(10)