/**
 * Task - 01
 *
 * Create any text element and container with 5 buttons inside.
 *
 * The task is to show inner text of the button that was just pressed.
 * Only one event listener can be used.
 * */

const parent = document.getElementById('root')

const text = document.createElement('p')

const createMoreBtn = (count = 0, container) => {
	if (count < 0) return null

    for (let i = 0; i < count; i++) {
        const button = document.createElement('button')
        button.innerText = `element - ${i}`

        container.append(button)
    }
}

parent.addEventListener('click', function(event) {
    // console.dir(event.target);
    if(event.target.tagName === 'button') {
        text.textContent = event.target.textContent
    }
    // if( event.target !== event.currentTarget)
})


window.addEventListener('DOMContentLoaded', () => {
    parent.prepend(text)
    createMoreBtn(5, parent)
})