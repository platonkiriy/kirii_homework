/**
 * Task - 02
 *
 * Create your custom modal window.
 *
 * CSS and markup are placed in html and scss files of this classwork.
 *
 * The modal window should appears after click on button "Show modal".
 *
 * Modal window should close after clicking "cross" btn on the top right corner of it, or after clicking anywhere on the screen except the modal itself.
 * */
// example 1

// const showModalBtn = document.createElement('button')
// showModalBtn.textContent = 'Show modal'
//
// document.getElementById('root').append(showModalBtn)
// const modal = document.querySelector('.modal-wrapper')
//
// showModalBtn.addEventListener('click', () => {
//     modal.style.display = 'flex'
//
//     modal.addEventListener('click', function (event) {
//         if (event.target === this || event.target.classList.contains('modal-close')) {
//             this.style.display = 'none'
//         }
//     })
// })

//example 2

const showModalBtn = document.createElement('button');
showModalBtn.textContent = 'Show modal';

document.getElementById('root').append(showModalBtn);

showModalBtn.addEventListener('click', () => {
  const modal = createModal('This is my content');

  document.body.append(modal);
});

function createModal(content) {
  const modalWrapper = document.createElement('div');

  modalWrapper.className = 'modal-wrapper';
  modalWrapper.insertAdjacentHTML(
    'afterbegin',
    `<div class="modal">
                    <button class="modal-close">x</button>
                    <p class="modal-text">
                        ${content}
                    </p>
                </div>`
  );

  modalWrapper.addEventListener('click', function (event) {
    if (
      event.target === this ||
      event.target.classList.contains('modal-close')
    ) {
      this.remove();
    }
  });
  return modalWrapper;
}
