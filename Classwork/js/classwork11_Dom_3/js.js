/*TASK - 1
 * Show alert with message 'This is click', after the click on the 'Click me' button.
 */

// const btn = document.createElement('button')
// btn.textContent = 'Click me'
// document.body.appendChild(btn)
// btn.addEventListener('click', () => alert('This is click'))


/*TASK - 2
* Show alert with message 'This is mouseover', after the hover on the 'Click me' button.
*/
// example1
// const btn = document.createElement('button')
// const text = document.createElement('p')
//
// btn.textContent = 'show text'
// text.textContent = 'this is text'
//
// document.body.prepend(btn, text)
//
// text.style.cssText = 'opacity:0; transition:0.5s'
//
// btn.addEventListener('mouseover', () => {
//     text.style.opacity = 1
// })
//
// btn.addEventListener('mouseout', () => {
//     text.style.opacity = 0
// })

// example2
// const btn = document.createElement('button')
// const text = document.createElement('p')

// btn.textContent = 'show text'
// text.textContent = 'this is text'

// document.body.prepend(btn, text)

// text.style.cssText = 'opacity:0; transition:0.5s'

// btn.addEventListener('mouseover', handleOpacity(1))

// btn.addEventListener('mouseout', handleOpacity(0))

// function handleOpacity(value) {
// 	return () => {
// 		text.style.opacity = value
// 	}
// }


/*TASK - 3
 * Create a function, that will be changing the background color of the 100px square randomly,
 * by the clicking on it. Every color should be random, transparent and white are not included in the list of colors.
 */

// const square = document.createElement('div');
// square.style.cssText = 'width: 100px; height: 100px; background-color: black;'
// document.body.prepend(square)
// square.addEventListener('click', () => {
//     let rgb = `rgb(${getRgbColor()}, ${getRgbColor()}, ${getRgbColor()})`
//     square.style.backgroundColor = rgb
// })
//
// const getRgbColor = () => Math.floor(Math.random() * 255)


/*TASK - 5
 * There is an input next to the 100px square.
 * Only HEX of the color can be entered in this input. Next to this input - 'Ok' btn is placed and its inactive by default.
 * Create a function that will be changing the color.
 * After hex is entered 'Ok' button should become active so user could press it.
 * After pressing the 'Ok' button color should changes.
 * */

function createElement(tag, content = '', styles = '') {
    const elem = document.createElement(tag)
    if (content) {
        elem.textContent = content
    }
    if (styles) {
        elem.style.cssText = styles
    }
    return elem
}

const square = createElement('div', '', 'width: 100px; height: 100px; background-color: black')
const field = createElement('input')
field.type = 'color'
const button = createElement('button', 'Ok')

document.body.prepend(square, field, button)

button.addEventListener('click', () => {
    square.style.backgroundColor = field.value
})
