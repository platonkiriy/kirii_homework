/*TASK 1
 * Write a function customCharAt(string,index)
 *   string - source string
 *   index - the index of the particular character of the string that we need
 * Return value: the character itself, that is placed on the specific index
 * */

// function customCharAt(string, index) {
//     return string.charAt(index);
// }
//
// console.log(customCharAt('specific', 5))


/*TASK 2
 * Create a function. It should receive 1 argument - source string.
 *
 * Turn all of the odd characters of the string in to UPPERCASE.
 *
 * Return value: string with uppercase odd characters
 */

// function upperOdd(str) {
//     let result = ''
//
//     for (let i = 0; i < str.length; i++) {
//         if (i % 2 === 0) {
//             result += str.charAt(i).toUpperCase();
//         } else {
//             result += str.charAt(i).toLowerCase()
//         }
//     }
//     return result
// }
//
// console.log(upperOdd('lorem ipsum dolor sit amet, consectetur adip'))

/* TASK 3
* Create a function cutMaxLength(string, maxlength)
* You need to cut the exact number of characters(maxLength) from source string and + "...".
* Return value:
*       if the number of the characters in source string is bigger then maxLength - the string that has been cut
*       if the number of the characters in source string is smaller then maxLength - the source string itself
*/
//
// function cutMaxLength(string, maxlength) {
//     if (string.length > maxlength) {
//         return string.slice(maxlength) + '...';
//     } else {
//         return string;
//     }
// }
//
// console.log(cutMaxLength("lqhbvlqehrbv lqherbv eqhjrbv lkjhbqer lvhb", 10))


/* TASK 4
 * Create a function getDayAgo(numberOfDays)
 *
 * Return value: name of the weekday, that was numberOfDays days before.
 */

// function getDayAgo(numberOfDays) {
//     const currentDate = new Date();
//     currentDate.setDate(currentDate.getDate() - numberOfDays)
//     const  indexOfTheDay = currentDate.getDay()
//
//     const weekDays = {
//         0: 'воскресенье',
//         1: 'понедельник',
//         2: 'вторник',
//         3: 'среда',
//         4: 'четверг',
//         5: 'пятница',
//         6: 'суббота',
//     }
//     return weekDays[indexOfTheDay]
// }
//
// console.log(getDayAgo(4))

/*TASK 5
* Create a function that takes string with date 'DD/MM/YYYY' as an argument.
*
* Return value: number of weekday of the first day in this month
* */

function getFirstDayinMoth(data) {
    const formatDate = data.split('/').reverse().join('.')

    const date = new Date(formatDate)

    date.setDate(1)

    const weekDays = {
        0: 'воскресенье',
        1: 'понедельник',
        2: 'вторник',
        3: 'среда',
        4: 'четверг',
        5: 'пятница',
        6: 'суббота',
    }
    return weekDays[date.getDay()]
}

console.log(getFirstDayinMoth('4/12/2021'))