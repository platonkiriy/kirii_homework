/* ЗАДАНИЕ - 1
 * Пользователь должен ввести Имя, Фамилию и свой возраст.
 * В случае если он вводит не соответствующие данные, нужно переспрашивать его, ДО ТЕХ ПОР ПОКА данные не будут введены корректно.
 * */

// let userName = prompt('enter your name')
// while (!isNaN(userName)) {
//     userName = prompt('please enter correct name')
// }
// alert(userName)
//
// let userLastName = prompt('enter your last name')
// while (!isNaN(userLastName)){
//     userLastName = prompt('please enter correct last name')
// }
// alert(userLastName)
//
// let userAge = prompt('enter your age')
// debugger
// while (typeof parseInt(userAge) !== 'number' || isNaN(parseInt(userAge))) {
//     userAge = prompt('please enter correct age')
// }
// alert(userAge)

/* ЗАДАНИЕ - 2
*    Вывести в консоль первые 147 непарных чисел.
 *   ПРОДВИНУТАЯ СЛОЖНОСТЬ - не выводить в консоль те числа, которые делятся на 5.
 * */
// let oddCounter = 0
// let numberResult = parseInt(prompt('how much numbers you want to see'));
// let i = parseInt(prompt('since what number to start'));
// while (oddCounter !== numberResult) {
//     if (i % 2 !== 0 && i % 5 !== 0) {
//         console.log(i)
//         oddCounter++
//     }
//     i++
// }

/* ЗАДАНИЕ - 3
 * Осуществляем проверку на корректность введения данных.
 * Пользователь должен ввести два числа и операцию.
 * Если пользователь ввел НЕ числа или операцию, которой нет в списке - спрашиваем все по новой, ДО ТЕХ ПОР ПОКА не введет правильно.
 *  Список операций:
 *   * - умножение
 *   + - добавление
 *   - - вычетание
 *   / - деление
 * */

let firstNum = parseFloat(prompt('enter first num'));
while (isNaN(firstNum) || firstNum === 0) {
    firstNum = parseFloat(prompt('enter correct first num'));
}

let secondNum = parseFloat(prompt('enter second num'));
while (isNaN(secondNum) || secondNum === 0) {
    secondNum = parseFloat(prompt('enter correct second num'));
}

let operation = prompt('enter operation');
while (operation !== "*" && operation !== "+" && operation !== "-" && operation !== "/") {
    debugger
    operation = prompt('enter correct operation');
}

switch (operation) {
    case "*":
        alert(firstNum * secondNum)
        break;
    case "/":
        alert(firstNum / secondNum)
        break;
    case "+":
        alert(firstNum + secondNum)
        break;
    case "-":
        alert(firstNum - secondNum)
        break;
}