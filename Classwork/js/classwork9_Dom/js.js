/* TASK - 1
 * Get several elements from the page, by:
 *   tag
 *   class
 *   identifier
 *   CSS selector
 * Use console.dir() method to show up the elements
 * */

// let tag = document.getElementsByTagName('ul')
// let className = document.getElementsByClassName('training-list')
// let identifier = document.getElementById('list')
// let cssSelector = document.querySelector('.training-list')
//
// console.log(tag)

/* TASK - 2
 * Create a function, that will:
 * take an elements from the page with class 'training-list', and text content equals 'list-element 5'.
 * Show this element in console.
 * Replace text content in this element to "<p>Hello</p>" without creating a new HTML element on the page
 * Use array methods to complete the task.
 * */

// function updateContentList() {
//     const itemsList = document.querySelectorAll('.training-list')
//     itemsList.forEach( value => {
//         if (value.textContent === 'list-element 5') {
//             value.innerHTML = '<p>Hello</p>'
//         }
//     })
// }
//
// updateContentList()

/* TASK - 3
 * Get an element with class 'remove-me' and remove it from the page.
 * Find element with class 'make-me-bigger'. Replace class 'make-me-bigger' to 'active'. Class 'active' already exists in CSS.
 * */

// let removeButton = document.querySelector('.remove-me')
// let biggerButton = document.querySelector('.make-me-bigger')
// removeButton.remove();
// // removeButton.style.display = 'none';
// // removeButton.outerHTML = '';
//
// // biggerButton.setAttribute('class', 'active');
// // biggerButton.className = 'active';
// biggerButton.classList.add('active')


/* TASK - 4
 * There is a list of items on the user screen.
 * Create a function that will find the 'run out' items. The amount of 'run out' items is equal to 0.
 * Replace the 0 inside text content of those elements to 'run out' and change the text color to red.
 */

//example1
function checkCountGood() {
    const list = document.querySelectorAll('.storage-item');
    list.forEach(item => {
        const splitContent = item.textContent.split(' - ')
        if (splitContent[1] == 0) {
            item.style.color = 'red';
            item.textContent = `${splitContent[0]} - run out`

        }
        console.log(splitContent)
    })
}

checkCountGood()

// example tbd
// function checkCounter() {
//     const elements = document.querySelectorAll('.storage')
//     elements.forEach(value => {
//         if (value.textContent.includes('- 0')){
//             return  value.textContent.replace('- 0', '- run out')
//         }
//     })
// }

// checkCounter()

/* TASK - 5
 * Create a function that will ask the user what exact item he wants to change.
 * User needs to write only item name. If there are no matches in the list
 * keep asking until the item name becomes valid.
 * Then ask for a new amount of selected item.
 * Show data entered by the user on the page.
 * Example:
 * item name - 'Coffee',
 * new amount of it - '17'
 * It means that you need to change only the amount of 'Coffee' and it should be 17 now.
 * */

// function changeQty(key, value) {
//     const list = document.querySelectorAll('.storage-item')
//
//     list.forEach(item => {
//         const splitContent = item.textContent.split(' - ')
//
//         if (splitContent[0] === key) {
//             item.textContent = `${splitContent[0]} - ${value}`
//         }
//     })
// }
//
// changeQty('Coca-cola', 10)
// changeQty('100% vegan no gluten herbal tea', 100000)