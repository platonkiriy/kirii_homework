/* TASK - 1
 * Create sum() function.
 * Arguments: two integer numbers
 * Return value: sum of two arguments
 * */

const summ = (firstNum, SecondNum) => firstNum + SecondNum

/* TASK - 2
* Write a functions, that will be counting from start to end of range.
* Arguments: start of the range, end of the range
*/

const range = (start, end) => {
    debugger
    if (!isNaN(start) && !isNaN(end)) {
        if (start > end) {
            for (let i = start; i <= end; i++) {
                console.log(i)
            }
        } else {
            for (let i = end; i <= start; i++) {
                console.log(i)
            }
        }
    }
}
range(5, 10);


/* TASK - 3
 * Write a function, that will sum up all the arguments passed into it.
 * */

// const summAllArgs = (...args) => {
//     let result = 0
//     for (let singleNumber of args) {
//         result += singleNumber
//     }
//     return result
// }
//
// console.log(summAllArgs(12,45,544,45,6,7,567,56,45,34,324,2))

/* TASK - 5
 * Create Arrow function, that returns maximum value from all of the arguments that was passed.
 * The number of arguments can be any. We need to return the biggest one.
 * */
//
// const maxValue = (...args) => Math.max(...args)
// console.log(maxValue(12,45,544,45,6,7,567,56,45,34,324,2))