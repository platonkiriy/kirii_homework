const form = document.getElementById('createEvent')
const title = document.getElementById('eventTitle')
const date = document.getElementById('eventDate')
const desc = document.getElementById('eventDesc')
const btn = document.querySelector('.event-form button')

function makeEventObject(title, date, desc) {
    console.log(title.value)
    console.log(date.value)
    console.log(desc.value)
    /** ЗАДАНИЕ
     * реализовать код функции  makeEventObject
     * эта функция принимает 3 аргумента - обьекты инпутов формы
     *  чтобы получить значние какого либо инпута достаточно обратиться к его свойству value
     *
     * Возвращаемое значение функции:
     *  обьект в котором должны быть 3 свойства -
     *    - title - это значение инпута title, каждый 3-й символ которого приведен в верхний регистр
     *    - daysBefore - количество дней, сколько осталось до даты события
     *    - description - значение инпута desc, в котором удалено каждое слово "типа"
     *
     * В решении нужно обязательно испольовать стрелочные функции */

    const makeTitle = (string) => {
        let result = ''
        for (let i = 1; i < string.length; i++) {
            if (i % 3 === 0) {
                result += string.charAt(i).toUpperCase();
            } else {
                result += string.charAt(i).toLowerCase()
            }
        }
        return result
    }

    const makeDays = (date) => {
        const currentDate = new Date();
        const futureDate = new Date(date)
        return Math.floor ((futureDate.getTime() - currentDate.getTime()) / (24 * 60 * 60 * 1000))
    }

    const makeDescription = (string) => {

        return string.split('типа').join('')
    }


    return {
        title: makeTitle(title.value), // makeTitle(title.value)
        daysBefore: makeDays(date.value), // makeDaysBefore(date.value)
        description: makeDescription(desc.value), // makeDescription(desc.value)
    }

}

btn.disabled = !title.value || !date.value || !desc.value
form.onchange = e => {
    btn.disabled = !title.value || !date.value || !desc.value
}

form.onsubmit = e => {
    e.preventDefault()
    const list = document.querySelector('.events-list')

    const event = makeEventObject(title, date, desc)

    list.insertAdjacentHTML(
        'beforeend',
        `
  <div class="events-list-item">
    <p class="event-list-title">${event.title}</p>
    <p class="event-list-days">${event.daysBefore}</p>
    <p class="event-list-desc">${event.description}</p>
  </div>
  `,
    )
}
