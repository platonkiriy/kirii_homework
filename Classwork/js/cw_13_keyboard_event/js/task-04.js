/**
 * Task - 04
 *
 * Recreate a pack-man on the page. And make it movable.
 *
 * Make user controls direction of pack-man movement by pressing W, S, A, D keys.
 *
 *  W - pack-man moves 20px to top from it's current position;
 *  S - pack-man moves 20px to bottom from it's current position;
 *  A - pack-man moves 20px to the left from it's current position;
 *  D - pack-man moves 20px to the right from it's current position;
 * */

const packman = document.querySelector('.packman')
packman.style.cssText = 'position: absolute; top: 0; left: 0;'

let x = 0
let y = 0

function movePackman() {
    window.addEventListener('keydown', function (e) {
        if (e.key === 'd') {
            if (x < window.innerWidth - 150){
            x += 20
            }
            packman.style.left = x + 'px'
            packman.style.transform = 'rotate(0deg)'
        }
        if (e.key === 'a') {
            if (x >= 0){
            x -= 20}
            packman.style.left = x + 'px'
            packman.style.transform = 'rotateY(180deg)'
        }
        if (e.key === 's') {
            if (y < window.innerHeight - 150) {
                y += 20
            }
            packman.style.top = y + 'px'
            packman.style.transform = 'rotate(90deg)'
        }
        if (e.key === 'w') {
            if (y >= 0){
            y -= 20}
            packman.style.top = y + 'px'
            packman.style.transform = 'rotate(-90deg)'
        }
    })
}

movePackman()