/* ЗАДАНИЕ 1
 * Записать в переменную '123', вывести в консоль typeof этой переменной.
 * Преобразовать эту переменную в численный тип при помощи parseInt(), parseFloat(), унарный плюс +
 * После этого повторно вывести в консоль typeof этой переменной.
 * */

// let stringValue = "123.1wece.45sdf";
// console.log(typeof stringValue, stringValue);
// let intValueParsInt = parseInt(stringValue);
// console.log(typeof intValueParsInt, intValueParsInt);
// let intValueParsFloat = parseFloat(stringValue);
// console.log(typeof intValueParsFloat, intValueParsFloat);
// let intValuePlus = +stringValue
// console.log(typeof intValuePlus, intValuePlus);

/* ЗАДАНИЕ 2
 * Вывести на экран уведомление с текстом "Hello! This is alert" при помощи модального окна alert
 * */

// const message = "Hello! This is alert";
// alert(message);

/* ЗАДАНИЕ 3
 * Вывести на экран модальное окно prompt с сообщением "Enter the number".
 * Результат выполнения модального окна записать в переменную, значение которой вывести в консоль.
 * */
// let result = prompt('Enter the number');
// console.log(result);

/* ЗАДАНИЕ 4
 * При помощи модального окна prompt получить от пользователя два числа.
 * Вывести в консоль сумму, разницу, произведение, результат деления и остаток от деления их друг на друга.
 * */
// let firstNumber = parseInt( prompt('enter first number'))
// let secondNumber = parseInt(prompt('enter second number'))
// console.log('summ = ', intFirstNumber + intSecondNumber)
// console.log('difference = ', intFirstNumber - intSecondNumber)
// console.log('multiply = ', intFirstNumber * intSecondNumber)
// console.log('division = ', intFirstNumber / intSecondNumber)
// console.log('remainder = ', intFirstNumber % intSecondNumber)


/* TASK 5
 * Use browser modal windows for getting three numbers from user.
 * Then execute into console:
 *   - arithmetic average
 *   - max number
 *   - min number
 * */

// let first = parseInt(prompt('enter first number'))
// let second = parseInt(prompt('enter second number'))
// let third = parseInt(prompt('enter third number'))
//
// let average = ((first + second +third) / 3)
// let max = Math.max(first, second, third)
// let min = Math. min(first, second, third)
//
// console.log('average = ', Math.round(average), 'max = ', max, 'min = ', min)

//
// /* TASK 1
// * Show up next things in console, using console.log() method:
//   - max integer value
//   - min integer value
//   - not a number
//   - max safe integer
//   - min safe integer
// * */
//
// let intValue = Math.random();
// const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
//
// function generateString(length) {
//     let result = ' ';
//     const charactersLength = characters.length;
//     for (let i = 0; i < length; i++) {
//         result += characters.charAt(Math.floor(Math.random() * charactersLength));
//     }
//
//     return result;
// }
//
// let stringValue = generateString(10)
//
// console.log(intValue + stringValue) // concatenation of 2 strings as + works as concatenation for string with other value types
// console.log('difference = ', intValue - stringValue)// Not a number as we cannot differ different types of values except the case when string nuber content only numbers, in this case String value will be modified to a number and after that executed action with numbers
// console.log('multiply = ', intValue * stringValue) // Not a number as we cannot multiply different types of values except the case when string nuber content only numbers, in this case String value will be modified to a number and after that executed action with numbers
// console.log('division = ', intValue / stringValue) // Not a number as we cannot divide different types of values except the case when string nuber content only numbers, in this case String value will be modified to a number and after that executed action with numbers
// console.log('remainder = ', intValue % stringValue) // Not a number as we cannot divide different types of values except the case when string nuber content only numbers, in this case String value will be modified to a number and after that executed action with numbers
//
// /* TASK 3
//  * Create 2 variables. Assign true for first one, and false for second one.
//  * Execute into console:
//  *   - result of comparing 0 and variable with false inside with == operator
//  *   - result of comparing 1 and variable with true inside with === operator
//  *   - Explain the result
//  * */
//
// let booleanTrue = true
// let booleanFalse = false
//
// console.log(0 == booleanFalse)//In JavaScript “0” is equal to false because when it tested for equality the automatic type conversion of JavaScript comes into effect and converts the “0” to its numeric value which is 0 and as we know 0 represents false value.
// console.log(1 === booleanTrue)//The === type equality operator doesn't do type conversions, so 1 === true evaluates to false because the values are of different types.
//
//
// // next task
//
// let x = 6
// let y = 15
// let z = 4
//
// console.log(x += y - x++ * z)
