/* ЗАДАНИЕ - 1
 * Написать функцию, которая принимает 2 аргумента - имя и возраст пользователя
 * Возвращаемое значение этой функции - объект с двумя ключами name и age, куда будут записаны значения переданных функции аргументов.
 * */

//example1
// function createUser(userName, userAge) {
//     const user = {
//         name: userName,
//         age: userAge,
//     }
//     return user
// }

//example2
// function createUser(userName, userAge) {
//     return  {
//         userName,
//         userAge,
//     }
// }

//example3
// const  createUser = (name, age) => ({name,age,})
//
// const newUser = createUser(prompt('enter name'), +prompt('enter age'))
// console.log(newUser)

/* ЗАДАНИЕ - 2
 * Добавить к предыдущему заданию функционал.
 * В возвращаемом объекте должен быть метод, который увеличивает возраст на 1.
 * Т.е. внутри объекта будет свойство\ключ\поле, значением которого будет являться функция,
 * которая увеличивает свойство\ключ\поле age ЭТОГО объекта на 1
 * */

// function createUser(userName, userAge) {
//     return  {
//         userName,
//         userAge,
//         increaseUserAge(){
//             this.userAge++
//         }
//     }
// }
//
// const newUser = createUser(prompt('enter name'), +prompt('enter age'))
// newUser.increaseUserAge()
// console.log(newUser)


/* ЗАДАНИЕ - 3
 * Добавить к предыдущему заданию функционал.
 * В возвращаемом объекте должен появиться еще один метод - addField(). Он будет добавлять свойства в объект.
 * Т.е. внутри объекта будет еще одно свойство\ключ\поле, значением которого будет являться функция.
 * Эта функция принимает два аргумента:
 *   1 - имя свойства, которое будет создаваться
 *   2 - значение. которое туда должно быть записано
 * */

// function createUser(userName, userAge) {
//     return {
//         userName,
//         userAge,
//         increaseUserAge() {
//             this.userAge++
//         },
//         addField(key, value) {
//             this[key] = value
//         }
//     }
// }
//
// const newUser = createUser(prompt('enter name'), +prompt('enter age'))
// newUser.increaseUserAge()
// newUser.addField("hobby", 'football')
// console.log(newUser)


/* ЗАДАНИЕ - 4
* Написать функцию createProductCart(). Которая создает объект карточки товара.
*
* Аргументы:
*  - title - название товара
*  - code - артикул товара
*  - price - цена
*  - description - описание
*
* Возвращаемое значение: объект карточки товара со всеми свойствами и методами
*
* Внутри функции нужно создать объект, записать в него све свойства и добавить один метод - startSale(), который изменяет цену товара и делает ее меньше на указанное количество процентов.
* метод startSale принимает аргумент - размер скидки, одним числом без знака процентов.
* */

function createProductCart(title, code, price, description) {
    return {
        title,
        code,
        _price: price,
        description,
        startSale(discount) {
            this._price = price - price * discount / 100
        },
        get price() {
            return `$ ${this._price}`
        },
        set price(value) {
            if (typeof value === 'number') {
                this._price = value
            }
        },

    }
}

const productCart = createProductCart('iphone', '12345', '100', 'apple device')
productCart.startSale(4)
productCart.price = 800
console.log(productCart.price)
console.log(productCart)

/* ЗАДАНИЕ - 5
* Дополнить функционал решения из прошлой задачи.
* Добавить для свойства price getter и setter.
* getter должен возвращать свойство в виде строки в формате `${ЗНАК ВАЛЮТЫ}${ЦЕНА}`, где знаком валюты должен быть знак гривны.
* */


