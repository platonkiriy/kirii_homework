const products = [
    {
        id: 'pr01',
        name: 'cup',
        price: 2,
        amount: 200,
        description: 'The ideal souvenir to remind the fan in the early morning, where his heart belongs'
    },
    {
        id: 'pr02',
        name: 'scarf',
        price: 4,
        amount: 150,
        description: 'A must for every staduim visit, not only in winter.'
    },
    {
        id: 'pr03',
        name: 'flag',
        price: 9,
        amount: 100,
        description: 'Supply your fans with flags in your colors. This will guarantee a sea of flags in your fan sector.'
    }
]
