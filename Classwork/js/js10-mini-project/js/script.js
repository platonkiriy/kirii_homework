function sortArr(arr, key, sortingType) {
  return arr.sort((elem1, elem2) => {
    switch (sortingType) {
      case "Asc":
        return elem1[key] - elem2[key];
      case "Desc":
        return elem2[key] - elem1[key];
    }
  });
}

// console.log(sortArr(products, 'price', 'Asc'));

function filterArray(arr, key, filter) {
  return arr.filter((elem) => elem[key].indexOf(filter) > -1);
}

// console.log(filterArray(products, 'description', 'fans'));

function countTotalPrice(orderId) {
  let totalPrice = 0;
  filterArray(orders, "id", orderId)[0].contains.forEach((product) => {
    totalPrice +=
      filterArray(products, "id", product.productId)[0].price * product.amount;
  });
  return totalPrice;
}

// console.log(countTotalPrice("o02"));

function addParam(path, obj) {
  const id = path[path.length - 1].id;
  const idChars = id
    .split("")
    .filter((el) => !Number.isInteger(parseInt(el)))
    .join("");
  const idNumber = id
    .split("")
    .filter((el) => Number.isInteger(parseInt(el)))
    .join("");

  parseInt(idNumber) < 9
    ? (obj.id = `${idChars}0${parseInt(idNumber) + 1}`)
    : (obj.id = idChars + parseInt(idNumber) + 1);
  path.push(obj);
  return path;
}

// debugger;
// console.log(addParam(orders, {}));

function modifyItem(path, id, key, keyValue) {
  const obj = filterArray(path, "id", id)[0];
  obj[key] = keyValue;
  return obj;
}

console.log(modifyItem(products, "pr01", "price", 5));
