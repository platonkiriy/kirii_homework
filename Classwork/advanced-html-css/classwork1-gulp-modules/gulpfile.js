const gulp = require('gulp'),
  concat = require('gulp-concat'),
  browserSync = require('browser-sync').create();

// gulp.task('buildHtml',  () =>
// 	gulp
// 		.src('./src/index.html')
// 		.pipe(gulp.dest('./prod/')),
// )

/***** PATHS ****/

const path = {
  src: {
    css: './src/scss/*.scss',
    js: './src/js/*.js',
  },
  prod: {
    self: './prod/',
    css: './prod/scss/',
    js: './prod/js/',
  },
};

/***** FUNCTIONS ****/

const buildCss = () =>
  gulp
    .src(path.src.css)
    .pipe(concat('_main.scss'))
    .pipe(gulp.dest(path.prod.css))
    .pipe(browserSync.stream());

const buildJs = () =>
  gulp
    .src(path.src.js)
    .pipe(concat('main.js'))
    .pipe(gulp.dest(path.prod.js))
    .pipe(browserSync.stream());

const watcher = () => {
  browserSync.init({
    server: {
      baseDir: './',
    },
  });

  gulp.watch('./index.html').on('change', browserSync.reload);
  gulp.watch(path.src.css, buildCss).on('change', browserSync.reload);
  gulp.watch(path.src.js, buildJs).on('change', browserSync.reload);
};

/***** TASK ****/

gulp.task('dev', gulp.series(buildCss, buildJs, watcher));

// gulp.series
// gulp.parallel
