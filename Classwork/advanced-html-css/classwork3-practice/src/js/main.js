function handleBurger() {
  document.querySelector('.burger').addEventListener('click', () => {
    document.querySelector('.menu').classList.toggle('active');
  });
}

window.addEventListener('DOMContentLoaded', () => {
  handleBurger();
});
