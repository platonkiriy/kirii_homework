const menu = document.querySelector('.menu');
const burger = document.querySelector('.burger');

document.body.addEventListener('click', function (e) {
  e.preventDefault();
  if (e.target === burger) {
    menu.classList.toggle('active');
    menu.classList.contains('active')
      ? burger.setAttribute('src', './dist/img/menu-buttonX.svg')
      : burger.setAttribute('src', './dist/img/menu-button.svg');
  } else {
    menu.classList.remove('active');
    burger.setAttribute('src', './dist/img/menu-button.svg');
  }
});
