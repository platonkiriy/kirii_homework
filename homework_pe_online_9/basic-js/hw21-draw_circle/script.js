const drawCircleBtn = document.querySelector('.drawCircleBtn');

const input = document.createElement('input');
input.type = 'text';
input.placeholder = 'Enter radius in px';

const confirmBtn = document.createElement('button');
confirmBtn.innerText = 'Draw';

drawCircleBtn.addEventListener('click', (e) => {
  e.preventDefault();
  drawCircleBtn.after(confirmBtn);
  drawCircleBtn.after(input);
  drawCircleBtn.remove();
});

const container = document.createElement('div');
container.classList.add('container');
container.style.cssText = `
    display: flex;
    justify-content: start;
    flex-wrap: wrap;
`;

confirmBtn.addEventListener('click', (e) => {
  if (input.value) {
    for (let i = 0; i < 100; i++) {
      const circle = document.createElement('div');
      circle.classList.add('circle');
      circle.innerWidth = input.value + 'px';
      circle.innerHeight = input.value + 'px';
      circle.style.cssText = `
    box-sizing: border-box;
    border-radius: 50%;
    background-color: #${Math.floor(Math.random() * 1000)}; 
`;
      container.append(circle);
    }
    input.remove();
    confirmBtn.remove();
    document.body.append(container);
  }
});
