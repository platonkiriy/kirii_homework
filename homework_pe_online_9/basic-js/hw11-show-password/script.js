const icons = [...document.querySelectorAll('.icon-password')];
const inputs = document.querySelectorAll('input');
const submitBtn = document.querySelector('.btn');

icons.forEach((icon) => {
  icon.addEventListener('click', (e) => {
    if (e.target.classList.contains('fa-eye-slash')) {
      e.currentTarget.classList.replace('fa-eye-slash', 'fa-eye');
      inputs[icons.indexOf(e.currentTarget)].setAttribute('type', 'password');
    } else {
      e.currentTarget.classList.replace('fa-eye', 'fa-eye-slash');
      inputs[icons.indexOf(e.currentTarget)].setAttribute('type', 'text');
    }
  });
});

const errorMessage = document.createElement('span');
errorMessage.style.cssText =
  'color: red;' +
  'position: absolute;' +
  'left: 0;' +
  'bottom: 10px;' +
  'font-size: 0.75rem';
errorMessage.innerText = 'Passwords should match';

submitBtn.addEventListener('click', (e) => {
  e.preventDefault();
  if (inputs[0].value === inputs[1].value) {
    debugger;
    if (errorMessage) errorMessage.remove();
    alert('You are welcome!');
  } else {
    inputs[1].after(errorMessage);
  }
});
