// 1. Recursion is a process of calling itself

function getNumber() {
  let userInput = prompt('Enter a number');
  while (
    !Number.isInteger(Number(userInput)) ||
    String(userInput).trim() === '' ||
    userInput === null ||
    userInput < 0
    ) {
    userInput = prompt('Number should be 0 or greater', userInput);
  }
  return +userInput;
}

function factorialize(number) {
  return !number ? 1 : number * factorialize(number - 1);
}

function factorializeWithFor(number) {
  if (!number) {
    return 1;
  }
  for (let i = number - 1; i > 0; i--) {
    number *= i;
  }
  return number;
}

console.log(factorialize(getNumber()));
console.log(factorializeWithFor(getNumber()));
