// 1. DOM is a structured representation of the of the web page. Tree of nodes
//    and objects

// 2. innerHTML gets or sets the HTML or XML markup contained within the element.
//    innerText represents the rendered text content of a node and its descendants.

// 3. the best way to find the element is querySelector() for a single element or
//    querySelectorAll() for multiple elements, which returns a node list where
//    you can use forEach() method without any manipulation like with an HTMLCollection.
//    There are more methods to find an element like:
//    getElementById()
//    getElementsByClassName()
//    getElementsByName()
//    getElementsByTagName()

// 1.
const allParagraphs = document.querySelectorAll('p');
allParagraphs.forEach((el) => (el.style.backgroundColor = '#ff0000'));

// 2.
const optionList = document.getElementById('optionsList');
console.log(optionList);
console.log(optionList.parentElement);
optionList.childNodes.forEach((el) => {
  console.log(el.nodeName, el.nodeType);
});

// 3.
const testParagraph = document.querySelector('#testParagraph');
testParagraph.textContent = 'This is a paragraph';

// 4.
const mainHeaderChildren = document.querySelector('.main-header').children;
console.log(mainHeaderChildren);
Array.from(mainHeaderChildren).forEach((el) => el.classList.add('nav-item'));

// 5.
const sectionTitleList = document.querySelectorAll('.section-title');
sectionTitleList.forEach((el) => el.classList.remove('section-title'));
