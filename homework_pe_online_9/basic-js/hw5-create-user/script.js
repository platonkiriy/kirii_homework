//1. object method is an action which allows us to interact with the object like adding, updating, getting, deleting
// properties and values
//2. any type that js allows like Number, Boolean, String, bigInt, symbol, undefined, null, Object
//3. This mean that a variable that keeps an object doesn't keep values of the Object, but it does keep a reference to a
// memory where the object values are.

const firstNameMessage = 'Enter your first name';
const lastNameMessage = 'Enter your last name';

function getUserInput(message) {
  let userInput = '';
  do {
    userInput = prompt(message);
  } while (!userInput);
  return userInput.trim()[0].toUpperCase() + userInput.trim().substring(1);
}

function createNewUser() {
  let userFirstName = getUserInput(firstNameMessage);
  let userLastName = getUserInput(lastNameMessage);

  const newUser = {
    _userFirstName: userFirstName.trim(),
    _userLastName: userLastName.trim(),

    set userFirstName(value) {
      if (value) this._userFirstName = value;
    },
    set userLastName(value) {
      if (value) this._userLastName = value;
    },
    get userFirstName() {
      return this._userFirstName;
    },
    get userLastName() {
      return this._userLastName;
    },
    setFirstName(value) {
      this.userFirstName = value;
    },
    setLastName(value) {
      this.userLastName = value;
    },
    getUserFirstName() {
      return this.userFirstName;
    },
    getUserLastName() {
      return this.userLastName;
    },
    getLogin() {
      return (
        this.userFirstName[0].toLowerCase() + this.userLastName.toLowerCase()
      );
    },
  };
  return newUser;
}

const user = createNewUser();

console.log('Your login is: ' + user.getLogin());
console.log('Your original name is: ' + user.getUserFirstName());
user.setFirstName('NewName');
console.log('Your new name is: ' + user.getUserFirstName());
