// 1. forEach() takes each element of the array and do some actions with the elements, as a param should be passed a callBack function
// 2. const arr = [1,2,3,4] to clear it you can just set arr = [] :-) or you can remove each value
// 3. Array.isArray(variable) checks if a variable is array. Also you can check with variable.constructor === Array, or variable instanceof Array

const array = [
  'hello',
  'world',
  23,
  '23',
  null,
  undefined,
  '',
  [1, 2],
  true,
  false,
];

const filterBy = (arr, filter) =>
  array.filter((elem) => typeof elem !== filter);

console.log(filterBy(array, 'string'));
