//1. A function is a block of code that used to perform a particular task. You can write a function and use it as much times as
// needed without writing repeated code
//2. Passed arguments are used inside the function like variable values. This help us reuse the same function with
// different variable values.
//3. Return operator returns a result on that place where the function was called

let firstNumber = getNumber('Enter first number');
let secondNumber = getNumber('Enter second number');

function getNumber(message) {
  let userInput = prompt(message);
  while (
    isNaN(+userInput) ||
    String(userInput).trim() === '' ||
    userInput === null
  ) {
    userInput = prompt('Enter correct number', userInput);
  }
  return parseFloat(userInput);
}

function getOperation() {
  let operation = prompt('Enter operation +, -, * or /');
  while (
    operation !== '+' &&
    operation !== '-' &&
    operation !== '*' &&
    operation !== '/'
  ) {
    operation = prompt('Operation can be only +, -, * or /', operation);
  }
  return operation;
}

function performAction(firstNumber, secondNumber, operation) {
  switch (operation) {
    case '+':
      return firstNumber + secondNumber;
    case '-':
      return firstNumber - secondNumber;
    case '*':
      return firstNumber * secondNumber;
    case '/':
      if (secondNumber) {
        return firstNumber / secondNumber;
      } else {
        return "Can't divide to 0";
      }
  }
}

console.log(performAction(firstNumber, secondNumber, getOperation()));
