// 1. Primitive (Boolean, Null, Undefined, Number, BigInt, String, Symbol), Objects
// 2. (===) the strict equality operator always considers operands of different types to be different.
//    (==) it attempts to convert and compare operands that are of different types.
// 3. Operator performs some operation on single or multiple operands (data value) and produces a result.
//    there are several types of operators: Arithmetic, Comparison, Logical, Assignment, Conditional and Ternary

const ENTER_NAME_MESSAGE = 'Enter your name';
const ENTER_AGE_MESSAGE = 'Enter you age';
const NOT_ALLOWED_MESSAGE = 'You are not allowed to visit this website';
let name = prompt(ENTER_NAME_MESSAGE);
let age = +prompt(ENTER_AGE_MESSAGE);

if (typeof name === 'string') {
  name = name.trim();
}

while (name === '' || typeof name !== 'string' || age < 0 || isNaN(age)) {
  name = prompt(ENTER_NAME_MESSAGE, name);
  age = +prompt(ENTER_AGE_MESSAGE, age);
  if (typeof name === 'string') {
    name = name.trim();
  }
}

if (age < 18) {
  alert('You are not allowed to visit this website');
} else if (age >= 18 && age <= 22) {
  let bool = confirm('Are you sure you want to continue?');
  if (bool === true) {
    alert(`Welcome, ${name}`);
  } else {
    alert(NOT_ALLOWED_MESSAGE);
  }
} else {
  alert(`Welcome, ${name}`);
}
