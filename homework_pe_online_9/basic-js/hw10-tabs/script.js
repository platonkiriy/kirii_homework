//implementation without additional classes, attributes and ids
function switchTabs() {
  const tabs = document.querySelectorAll('.tabs-title');
  const tabsContent = document.querySelectorAll('.tabs-content li');

  tabs.forEach((tab, tabIdx) => {
    tabsContent.forEach((content, contentIdx) => {
      if (tabIdx === contentIdx && !tab.classList.contains('active')) {
        content.style.display = 'none';
      }
    });
    tab.addEventListener('click', () => {
      if (tab.classList.contains('active')) return;
      tabs.forEach((tab) => tab.classList.remove('active'));
      tabsContent.forEach((elem) => (elem.style.display = 'none'));
      tab.classList.add('active');
      tabsContent[tabIdx].style.display = 'block';
    });
  });
}

switchTabs();

// alternate implementation with adding data-toggle attribute to '.tabs-title' elements
// (i.e <li data-toggle="akali" class="tabs-title active">Akali</li>)
// and adding ids to '.tabs-content li' elements (i.e. <li id="akali" >some text</li>)
// to HTML document
function switchTabs1() {
  const tabs = document.querySelectorAll('.tabs-title');
  const tabsContent = document.querySelectorAll('.tabs-content li');

  tabs.forEach((element) =>
    element.addEventListener('click', () => {
      if (element.classList.contains('active')) return;
      tabs.forEach((elem) => elem.classList.remove('active'));
      tabsContent.forEach((elem) => (elem.style.display = 'none'));
      element.classList.add('active');
      document.getElementById(this.dataset.toggle).style.display = 'block'; //this.dataset.toggle equals event.target.getAttribute('data-toggle')
    })
  );
  tabs[0].click();
}
