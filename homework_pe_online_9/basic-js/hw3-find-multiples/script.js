// 1. We use loops in programming in order to perform some actions repeatedly depending on condition true/false
//    for example we can ask user to enter some data and repeat this action until user enters a valid data
//    other than that we can fill some data into an object using a loop, this will take much less time and code

// 2. 'for' is used for actions with known range or iterations
//    'do while' when you want to at least one iteration until condition meets expectation
//    'while' is the same as 'do while', but if the condition is met at once, actions won't be performed
//    'for in' is used for Object, but I didn't have a chance to try it
//    'for of' is used for arrays, Map, Set, Object, didn't have a chance to try it neither.

// 3. Implicit type casting - automatic type conversion, happens when JavaScript automatically converts one data type to another (using '+', '!', etc.)
//    Explicit Conversion - manual type conversion, happens when using built-in methods like String(), .toString(), Number(),
//    .parseInt(), Math.floor(), Boolean(), etc.

let number = prompt('Enter a number');
let result = '';

while (
  !Number.isInteger(Number(number)) ||
  String(number).trim() === '' ||
  number === null
) {
  number = prompt('Enter a number');
}
/* as boundary values are specified as a range "between 0 and entered number",
   I assume they should not be included */
if (Math.sign(number) * number > 5) {
  for (let i = 5; i < Math.sign(number) * number; i++) {
    if (i % 5 === 0) {
      result += Math.sign(number) * i + ', ';
    }
  }
  console.log(result);
} else console.log('Sorry, no numbers');

let m = parseInt(prompt('enter the first number'));
let n = parseInt(prompt('enter the second number'));
let primeNumbers = '';

while (m > n) {
  alert('the first number should be less than the second one');
  m = parseInt(prompt('enter the first number'));
  n = parseInt(prompt('enter the second number'));
}

for (m; m <= n; m++) {
  let flag = 1;
  if (m > 2 && m % 2 !== 0) {
    for (let j = 3; j * j <= m; j = j + 2) {
      if (m % j === 0) {
        flag = 0;
        break;
      }
    }
  } else if (m !== 2) flag = 0;
  if (flag === 1) {
    primeNumbers += m + ', ';
  }
}

console.log(primeNumbers);
