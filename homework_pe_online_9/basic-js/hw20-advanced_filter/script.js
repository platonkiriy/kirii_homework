const characters = [
  {
    name: 'Luke Skywalker',
    height: '172',
    mass: '77',
    hair_color: 'blond',
    skin_color: 'fair',
    eye_color: 'blue',
    birth_year: '19BBY',
    gender: 'male',
    homeworld: 'Tatooine',
    species: [],
    created: '2014-12-09T13:50:51.644000Z',
    edited: '2014-12-20T21:17:56.891000Z',
  },
  {
    name: 'C-3PO',
    height: '167',
    mass: '75',
    hair_color: 'n/a',
    skin_color: 'gold',
    eye_color: 'yellow',
    birth_year: '112BBY',
    gender: 'n/a',
    homeworld: {
      name: 'Tatooine',
      description: 'asd',
    },
    species: [],
    created: '2014-12-10T15:10:51.357000Z',
    edited: '2014-12-20T21:17:50.309000Z',
  },
  {
    name: 'R2-D2',
    height: '96',
    mass: '32',
    hair_color: 'n/a',
    skin_color: 'white, blue',
    eye_color: 'red',
    birth_year: '33BBY',
    gender: 'n/a',
    homeworld: [
      {
        name: 'Tatooine',
        description: 'asd',
      },
      {
        name: 'Eriadu',
        description: 'dsa',
      },
    ],
  },
  {
    name: 'Darth Vader',
    height: '202',
    mass: '136',
    hair_color: 'none',
    skin_color: 'white',
    eye_color: 'yellow',
    birth_year: '41.9BBY',
    gender: 'male',
    homeworld: 'Tatooine',
    films: [
      'A New Hope',
      'The Empire Strikes Back',
      'Return of the Jedi',
      'Revenge of the Sith',
    ],
    species: [],
    vehicles: [],
    starships: ['TIE Advanced x1'],
    created: '2014-12-10T15:18:20.704000Z',
    edited: '2014-12-20T21:17:50.313000Z',
  },
];

const isParentArray = (obj, path) => {
  const parentKey = path.split('.')[0];
  return Array.isArray(obj[parentKey]);
};

const parseObject = (obj, path) => {
  const splitted = path.split('.');
  if (isParentArray(obj, path)) {
    return obj[splitted[0]].map((element) => element[splitted[1]]);
  }
  return splitted.reduce((res, key) => res[key], obj);
};

function filterCollection(arr, filter, matchAllFilters, ...keys) {
  const foundItems = [];
  const filters = filter.split(' ');
  const reg = new RegExp('\\b' + filters.join('|') + '\\b', 'i');
  arr.filter((item) => {
    let shouldSkip = false;

    filters.forEach((filter) => {
      let filterCounter = 0;
      if (shouldSkip) return;

      keys.forEach((key) => {
        if (shouldSkip) return;
        const textValue = parseObject(item, key);

        const hasFilterValue = Array.isArray(textValue)
          ? textValue.some((text) => reg.test(text))
          : reg.test(textValue);

        if (hasFilterValue) {
          filterCounter++;
        }

        if (hasFilterValue) {
          if (matchAllFilters) {
            if (filterCounter === filters.length) {
              foundItems.push(item);
              filterCounter = 0;
              shouldSkip = true;
            }
          } else {
            foundItems.push(item);
            shouldSkip = true;
          }
        }
      });
    });
  });
  return foundItems;
}

console.log(
  filterCollection(
    characters,
    'Tatooine 112BBY',
    true,
    'homeworld.name',
    'birth_year'
  )
);
