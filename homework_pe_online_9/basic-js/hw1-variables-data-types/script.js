//1. You can declare a variable using var, let or const keywords

//2. "Confirm" shows a modal window with a question and 2 buttons (Ok/Cancel) and returns a boolean value true or false depending on a clicked button
//   "Prompt" shows a modal window with a question, text field and 2 buttons (Ok/Cancel) and returns a string value (empty or added text) true in case if Ok button was clicked, or null in case if Cancel button/(Esc key) was clicked/(pressed)

//3. Implicit type casting happens when JavaScript automatically converts one data type to another. This can be done with:
//      1. + sign and some string '' (will be converted to String),
//      2. + before some value will try to convert this value to Number
//      3. logical operator ! before some value will convert this value to boolean
//      4. numeric string used with - , / , * results number type
//      etc.

// 1.
const name = 'Platon';
let admin = name;
console.log(admin);

// 2.
let days = Math.floor(Math.random() * 10);
console.log(days * 24 * 60);

// 3.
console.log(prompt('Add smth', 'some text'));
