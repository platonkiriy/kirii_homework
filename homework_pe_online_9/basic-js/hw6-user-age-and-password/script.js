//1. The mirroring helps add to a string special characters without breaking the string value
//2. Function declaration (function name(param) {}) and function expression (const someFunction = function(param) {})
//3. Hoisting means moving the declaration of functions, variables to the top of their scope.
//   It lets you use a function before you declare it in your code (applicable for function declaration)
//   Variables declared with let and const are also hoisted but, unlike var, are not initialized with a default value undefined.
//   An exception will be thrown if a variable declared with let or const is read before it is initialized.

const firstNameMessage = 'Enter your first name';
const lastNameMessage = 'Enter your last name';
const ageMessage = 'Enter your age in format dd.mm.yyyy';

function getUserInput(message) {
  let userInput = '';
  do {
    userInput = prompt(message);
  } while (!userInput);
  return userInput.trim()[0].toUpperCase() + userInput.trim().substring(1);
}

function getUserDates(message) {
  const regex = new RegExp(
    '^(?:(?:31(\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\.)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$'
  );
  let userDate = getUserInput(ageMessage);
  let dateParts = userDate.split('.');
  let convertedDate = [dateParts[2], dateParts[1], dateParts[0]].toString();
  while (
    !regex.test(userDate) ||
    new Date(convertedDate).getTime() > new Date().getTime()
  ) {
    userDate = getUserInput(message);
    dateParts = userInput.split('.');
    convertedDate = [dateParts[2], dateParts[1], dateParts[0]].toString();
  }
  let dates = [userDate, convertedDate];
  return dates;
}

function createNewUser() {
  let userFirstName = getUserInput(firstNameMessage);
  let userLastName = getUserInput(lastNameMessage);
  let userBirthday = getUserDates(ageMessage);

  const newUser = {
    _userFirstName: userFirstName,
    _userLastName: userLastName,
    birthday: userBirthday[0],
    getAge() {
      return Math.floor((new Date() - new Date(userBirthday[1])) / 31536000000);
    },
    getPassword() {
      return (
        this._userFirstName[0].toUpperCase() +
        this._userLastName.toLowerCase() +
        this.birthday.slice(-4)
      );
    },
    set userFirstName(value) {
      if (value) this._userFirstName = value;
    },
    set userLastName(value) {
      if (value) this._userLastName = value;
    },
    get userFirstName() {
      return this._userFirstName;
    },
    get userLastName() {
      return this._userLastName;
    },
    setFirstName(value) {
      this._userFirstName = value;
    },
    setLastName(value) {
      this._userLastName = value;
    },
    getUserFirstName() {
      return this._userFirstName;
    },
    getUserLastName() {
      return this._userLastName;
    },
    getLogin() {
      return (
        this._userFirstName[0].toLowerCase() + this._userLastName.toLowerCase()
      );
    },
  };
  return newUser;
}

const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
