const firstNameMessage = 'Enter your first name';
const lastNameMessage = 'Enter your last name';
const lessonMessage = 'Enter lesson';
const markMessage = 'Enter mark';

function getUserInput(message) {
  let userInput = '';
  do {
    userInput = prompt(message);
  } while (!userInput);
  return userInput.trim()[0].toUpperCase() + userInput.trim().substr(1);
}

const student = {
  firstname: null,
  lastname: null,
  setTabel() {
    const tabel = {};
    let lesson = prompt(lessonMessage);
    if (lesson !== null) {
      let mark = +prompt(markMessage);
      while (isNaN(mark) || mark == 0) {
        mark = +prompt(markMessage);
      }
      while (lesson) {
        tabel[lesson] = mark;
        lesson = prompt(lessonMessage);
        if (lesson) {
          mark = +prompt(markMessage);
        }
      }
    }
    this.tabel = tabel;
  },
  getBadMarksCount() {
    let counter = 0;
    for (const objKey in this.tabel) {
      if (this.tabel[objKey] < 4) counter++;
    }
    return counter
      ? `Number of bad marks: ${counter}`
      : 'Student goes to the next course';
  },
  isStipend() {
    let sum = 0;
    for (const key in this.tabel) {
      sum += this.tabel[key];
    }
    let average = (sum / Object.keys(this.tabel).length).toFixed(2);
    return average > 7 ? `${average} Student gets a stipend` : average;
  },
};

student.firstname = getUserInput(firstNameMessage);
student.lastname = getUserInput(lastNameMessage);
student.setTabel();
console.log(student.getBadMarksCount());
console.log(student.isStipend());
console.log(student);
