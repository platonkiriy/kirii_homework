const velocity = [3, 2, 4, 2];
const tasks = [5, 13, 8, 13, 5, 3, 8, 8, 5, 5, 13];
const deadline = new Date('2022, 10, 14');

function getBusinessDaysFromTodayCount(endDate) {
  let count = 0;
  const curDate = new Date();
  while (curDate <= endDate) {
    const dayOfWeek = curDate.getDay();
    if (dayOfWeek !== 0 && dayOfWeek !== 6) count++;
    curDate.setDate(curDate.getDate() + 1);
  }
  return count;
}

function evaluateBacklog(velocity, tasks, deadline) {
  const totalVelocityPerDay = velocity.reduce(
    (startVal, currentVal) => startVal + currentVal
  );
  const totalSP = tasks.reduce((startVal, currentVal) => startVal + currentVal);
  const nDaysToDeadline =
    //Assuming that today is a working day and working day till 19-00
    (new Date(
      new Date().getFullYear(),
      new Date().getMonth(),
      new Date().getDate(),
      19
    ) -
      new Date()) /
      (24 * 60 * 60 * 1000) +
    getBusinessDaysFromTodayCount(deadline);
  const nDaysToCompleteTasks = totalSP / totalVelocityPerDay;
  nDaysToCompleteTasks < nDaysToDeadline
    ? alert(
        `Усі завдання будуть успішно виконані за ${Math.floor(
          nDaysToDeadline - nDaysToCompleteTasks
        )} днів до настання дедлайну!`
      )
    : alert(
        `Команді розробників доведеться витратити додатково ${Math.abs(
          Math.floor((nDaysToDeadline - nDaysToCompleteTasks) * 24)
        )} годин після дедлайну, щоб виконати всі завдання в беклозі`
      );
}

evaluateBacklog(velocity, tasks, deadline);
