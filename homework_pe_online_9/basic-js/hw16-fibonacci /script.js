function getNumber() {
  let userInput = prompt('Enter a number');
  while (
    !Number.isInteger(Number(userInput)) ||
    String(userInput).trim() === '' ||
    userInput === null
  ) {
    userInput = prompt('Number should be integer', userInput);
  }
  return +userInput;
}

function fibonacci(n) {
  if (!n || n === 1 || n === -1) return n;
  if (n > 1) {
    return fibonacci(n - 1) + fibonacci(n - 2);
  } else {
    return Math.pow(-1, n + 1) * fibonacci(Math.abs(n));
  }
}

console.log(fibonacci(getNumber()));
