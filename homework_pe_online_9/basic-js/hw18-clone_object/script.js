const obj = {
  items: [
    {
      id: 0,
      text: 'Item1',
      listValues: [{ item1: 'text', item2: 2 }, ['a', 'b', 'c'], 'text'],
    },
    {
      id: 1,
      text: 'Item2',
    },
  ],
};

function cloneObj(inObject) {
  if (typeof inObject !== 'object' || inObject === null) {
    return inObject;
  }
  let newObj = Array.isArray(inObject) ? [] : {};
  for (let key in inObject) {
    let value = inObject[key];
    newObj[key] = cloneObj(value);
  }
  return newObj;
}
debugger;
console.log(cloneObj(obj));
