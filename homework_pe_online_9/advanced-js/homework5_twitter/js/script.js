export const getUsers = async () => {
    const response = await fetch('https://ajax.test-danit.com/api/json/users')
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`)
    }
    return await response.json();
}

export const getPosts = async () => {
    const response = await fetch('https://ajax.test-danit.com/api/json/posts')
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`)
    }
    return await response.json();
}

export const deletePosts = async (postId) => {
    const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {method: 'DELETE'})
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`)
    }
    return response.status;
}
