import Cards from "./cards.js";

window.addEventListener("load", async () => {
    const container = document.querySelector('.main')
    const cards = new Cards();
    const cardsList = await cards.render()
    container.appendChild(cardsList)
})