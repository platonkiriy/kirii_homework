import {deletePosts} from "./script.js";

export default class Card {

    _addElement(el, className, innerText = '') {
        const element = document.createElement(el)
        element.classList.add(className)
        element.innerText = innerText
        return element;
    }

    _addDeleteBtn() {
        const deleteBtn = document.createElement('span')
        deleteBtn.innerHTML = '&#9587;'
        return deleteBtn;
    }

    render(cardData) {
        const wrapper = this._addElement('div', 'card')
        const userInfo = this._addElement('div', 'user-info')
        const userName = this._addElement('p', 'name', cardData.name)
        const userEmail = this._addElement('p', 'email', cardData.email)
        const postTitle = this._addElement('p', 'title', cardData.title)
        const userPost =this._addElement('p', 'post', cardData.body)
        const deleteBtn = this._addDeleteBtn()

        userInfo.append(userName, userEmail)
        wrapper.append(userInfo, postTitle, userPost, deleteBtn)

        deleteBtn.addEventListener('click',   async (e) => {
            await deletePosts(cardData.id)
            e.target.closest('.card').remove()
            deleteBtn.removeEventListener("click", ()=>{})
        })

        return wrapper
    }
}