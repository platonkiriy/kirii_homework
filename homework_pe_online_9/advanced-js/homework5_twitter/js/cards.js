import Card from "./card.js";
import {getUsers, getPosts} from "./script.js";

export default class Cards {

    async _getData() {
        const users = await getUsers()
        const posts = await getPosts()
        return posts.map((item) => {
            const user = users.find((user) => item.userId === user.id)
            return {...user, ...item}
        })
    }

    async render() {
        const data = await this._getData()
        const card = new Card()
        const wrapper = document.createElement('div')
        wrapper.classList.add('tweets-container')
        data.forEach((item) => {
             wrapper.appendChild(card.render(item))
        })
        return wrapper
    }
}