const container = document.querySelector('.container')

const addLoader = (element) => {
    const loader = document.createElement('div')
    const div = document.createElement('div')
    loader.append(div)
    loader.className = 'lds-circle'
    element.append(loader)
}

const getFilms = async () => {
    const response = await fetch('https://ajax.test-danit.com/api/swapi/films')
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`)
    }
    return  response.json();
}

const getAllPromises = async (urls) => {
    return await Promise.all(urls.map(async url => {
        const resp = await fetch(url);
        return resp.json();
    }));
}

const createFilmsAndReturnCharactersList = (films) => {

    const charactersList = new Map()
    films.forEach(film => {
        const itemWrapper = document.createElement('div')
        addLoader(itemWrapper)
        const item = document.createElement('div');
        itemWrapper.dataset.film = film.id
        itemWrapper.style.paddingTop = '20px'
        item.innerText = `episode: ${film.episodeId} Name: ${film.name} Description: ${film.openingCrawl}`
        itemWrapper.append(item)
        container.append(itemWrapper)
        charactersList.set(film.id, film.characters)
    })
    return charactersList
}

getFilms().then(films => {
    const charactersList = createFilmsAndReturnCharactersList(films)

    charactersList.forEach((urls, filmId) => {
        const names = document.createElement('div')
        names.style.paddingTop = '10px'
        names.style.fontStyle = 'italic'
        names.style.fontSize = '0.8rem'

        getAllPromises(urls).then(results => {
            document.querySelector(`div[data-film="${filmId}"]`).firstElementChild.remove()
            const characters = []

            results.forEach(person => {
                characters.push(person.name)
            })

            names.innerText = `Characters: ${characters}`
            document.querySelector(`div[data-film="${filmId}"]`).append(names)
        });
    })
})