// Asynchronous - multiple related things happening without waiting for the previous one to complete


const findBtn = document.querySelector('.btn')
const container = document.querySelector('.info')

const getIp = async () => {
    const response = await fetch('https://api.ipify.org/?format=json')
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`)
    }
    const data = await response.json();
    return data.ip
}

const getIpInfo = async (ip) => {
    const response = await fetch(`http://ip-api.com/json/${ip}?fields=status,continent,country,region,city,district,query`)
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`)
    }
    return await response.json();
}

const addInfo = () => {
    findBtn.addEventListener("click", async (e) => {
        const ip = await getIp()
        const ipInfo = await getIpInfo(ip)
        const {continent, country, region, city, district} = ipInfo
        container.innerText = `Continent: ${continent}, Country: ${country}, Region: ${region}, City: ${city}, District: ${district}`
    })
}

addInfo()