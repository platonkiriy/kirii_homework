// 1. After the property is added to an object prototype it becomes available for other child objects. With this inheritance
// we don't need to copy the property (method) every time when we crate a new instance of the object and just can use them.

// 2. A derived constructor must call super in order to execute its parent (base) constructor, otherwise the object for this
// won’t be created. And we’ll get an error.

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get salary() {
    return this._salary;
  }

  set name(newName) {
    newName = newName.trim();
    if (!newName) {
      throw 'Enter an Employee name';
    }
    this._name = newName;
  }

  set age(newAge) {
    if (!newAge) {
      throw 'Enter an Employee age';
    }
    this._age = newAge;
  }

  set salary(newSalary) {
    if (!newSalary) {
      throw 'Enter an Employee Salary';
    }
    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }

  get lang() {
    return this._lang;
  }

  set salary(newSalary) {
    if (!newSalary) {
      throw 'Enter an Employee salary';
    }
    this._salary = newSalary;
  }
}

const programmer1 = new Programmer('Vasya', 22, 1000, ['en', 'ua']);
console.log(programmer1);

const programmer2 = new Programmer('Petya', 30, 3000, ['en', 'ua', 'es']);
console.log(programmer2);
console.log(programmer2.salary);

const employee = new Employee('Kolya', 35, 7000);
console.log(employee);
