// 1. We can use try...catch for parsing response from server, handle JavaScript exceptions such as ReferenceError,
// SYntaxError, and any other case where potentially could be an error.

const books = [
  {
    author: 'Люсі Фолі',
    name: 'Список запрошених',
    price: 70,
  },
  {
    author: 'Сюзанна Кларк',
    name: 'Джонатан Стрейндж і м-р Норрелл',
  },
  {
    name: 'Дизайн. Книга для недизайнерів.',
  },
  {
    author: 'Алан Мур',
    name: 'Неономікон',
    price: 70,
  },
  {
    author: 'Террі Пратчетт',
    name: 'Рухомі картинки',
    price: 40,
  },
  {
    author: 'Анґус Гайленд',
    name: 'Коти в мистецтві',
  },
];

const container = document.querySelector('.root');
const props = ['author', 'name', 'price'];

function validateProps(book) {
  const missingProps = [];
  let isValid = true;

  props.forEach((el) => {
    if (!Object.keys(book).includes(el)) {
      missingProps.push(el);
      isValid = false;
    }
  });

  return {
    isValid,
    missingProps,
  };
}

function printBooks() {
  books.forEach((book) => {
    try {
      const validated = validateProps(book);
      if (validated.isValid) {
        const itemWrapper = document.createElement('ul');

        props.forEach((prop) => {
          const item = document.createElement('li');
          item.innerText = book[prop];
          itemWrapper.appendChild(item);
        });

        container.appendChild(itemWrapper);
      } else {
        throw validated.missingProps;
      }
    } catch (e) {
      console.log(`The book ${book.name} does not have ${e}`);
    }
  });
}

printBooks();
