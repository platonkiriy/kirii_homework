import {Component} from "react";
import './../../styles/button.scss'

export default class Button extends Component {
    render() {
        const {backgroundColor, text, onClick} = this.props;

        return (
            <button
                style={{backgroundColor: backgroundColor}}
                onClick={onClick}
                className='button'
            >
                {text}
            </button>
        );
    }
}