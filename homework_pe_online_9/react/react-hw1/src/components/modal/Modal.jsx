import {Component} from "react";
import './../../styles/modal.scss'

export default class Modal extends Component {

    componentDidMount() {
        window.addEventListener('click', this.handleOutsideClick);
    }

    componentWillUnmount() {
        window.removeEventListener('click', this.handleOutsideClick);
    }

    handleOutsideClick = (event) => {
        if (event.target === this.modalRef) {
            this.props.onClose();
        }
    };

    render() {
        const {header, closeButton, text, actions, onClose} = this.props

        return (
            <div className='modal' ref={node => this.modalRef = node}>
                <div className='modal-styles'>
                    <div className='modal-header'>
                        <h3>{header}</h3>
                        {closeButton && (
                            <span onClick={onClose}>
                                &times;
                            </span>
                        )}
                    </div>
                    <p className='modal-text'>{text}</p>
                    {actions && <div className='button-wrapper'>{actions}</div>}
                </div>
            </div>
        )
    }
}