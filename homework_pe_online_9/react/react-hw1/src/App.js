import Button from "./components/button/Button";
import Modal from "./components/modal/Modal";
import React, {Component, Fragment} from 'react';

export default class App extends Component {
    state = {
        firstModalOpen: false,
        secondModalOpen: false,
    };

    openFirstModal = () => {
        this.setState({
            firstModalOpen: true,
        });
    };

    openSecondModal = () => {
        this.setState({
            secondModalOpen: true
        });
    };

    closeFirstModal = () => {
        this.setState({
            firstModalOpen: false
        });
    };

    closeSecondModal = () => {
        this.setState({
            secondModalOpen: false
        });
    };

    render() {
        const { firstModalOpen, secondModalOpen } = this.state;

        return (
            <Fragment>
                <Button text='Open first modal' backgroundColor='green' onClick={this.openFirstModal} />
                <Button text='Open second modal' backgroundColor='blue' onClick={this.openSecondModal}/>

                {firstModalOpen && (
                    <Modal
                        header="Do you want to delete this file?"
                        closeButton={true}
                        onClose={this.closeFirstModal}
                        text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
                        actions={
                            <>
                                <Button backgroundColor="rgba(0, 0, 0, 0.2)" text="Ok" onClick={this.closeFirstModal} />
                                <Button backgroundColor="rgba(0, 0, 0, 0.2)" text="Cancel" onClick={this.closeFirstModal} />
                            </>
                        }
                    />
                )}

                {secondModalOpen && (
                    <Modal

                        header="Second Modal"
                        closeButton={false}
                        onClose={this.closeSecondModal}
                        text="This is the second modal."
                        actions={<Button text="Close" backgroundColor="green" onClick={this.closeSecondModal} />}
                    />
                )}
            </Fragment>
        );
    }
}
