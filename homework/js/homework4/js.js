//1. object method is an action which allows us to interact with the object like adding, updating, getting, deleting
// properties and values


const firstNameMessage = 'Enter your first name'
const lastNameMessage = 'Enter your last name'
function checkUserInput(userInput, message) {
    while (userInput === null || userInput.trim() === '') {
        userInput = prompt(message)
    }
    return userInput
}

function createNewUser() {
    let userFirstName = prompt('Enter your first name')
    userFirstName = checkUserInput(userFirstName, firstNameMessage)
    let userLastName = prompt('Enter your last name')
    userLastName = checkUserInput(userLastName, lastNameMessage)

    const newUser = {
        _userFirstName: userFirstName.trim(),
        _userLastName: userLastName.trim(),
        getLogin() {
            return this._userFirstName[0].toLowerCase() + this._userLastName.toLowerCase()
        },
        set userFirstName(value) {
            if (value !== '' && value !== null) {
                this._userFirstName = value
            }
        },
        set userLastName(value) {
            if (value !== '' && value !== null) {
                this._userLastName = value
            }
        },
        get userFirstName() {
            return this._userFirstName
        },
        get userLastName() {
            return this._userLastName
        },
        setFirstName(value) {
            if (value !== '' && value !== null) {
                this.userFirstName = value
            }
        },
        setLastName(value) {
            if (value !== '' && value !== null) {
                this.userLastName = value
            }
        },
        getUserFirstName() {
            return this.userFirstName
        },
        getUserLastName() {
            return this.userLastName
        }
    }
    return newUser
}


console.log(createNewUser().getLogin())
// createNewUser().setFirstName('Platon')



