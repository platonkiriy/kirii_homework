//1. mirroring helps add to a string special characters without breaking the string value


const firstNameMessage = 'Enter your first name'
const lastNameMessage = 'Enter your last name'
const ageMessage = 'Enter your age in format dd.mm.yyyy'

function checkUserInput(userInput, message) {
    while (userInput === null || userInput.trim() === '') {
        userInput = prompt(message)
    }
    return userInput
}

function checkDateInput(userInput, message) {
    const regex = new RegExp('^(?:(?:31(\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\.)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$')
    let dateParts = userInput.split('.')
    let convertedDate = [dateParts[2], (dateParts[1]), dateParts[0]].toString()
    while (!regex.test(userInput) || (new Date(convertedDate).getTime()) > (new Date().getTime())) {
        userInput = prompt(message)
        dateParts = userInput.split('.')
        convertedDate = [dateParts[2], (dateParts[1]), dateParts[0]].toString()
    }
    return userInput
}

function createNewUser() {
    let userFirstName = prompt(firstNameMessage)
    userFirstName = checkUserInput(userFirstName, firstNameMessage)
    let userLastName = prompt(lastNameMessage)
    userLastName = checkUserInput(userLastName, lastNameMessage)
    let userBirthday = prompt(ageMessage)
    userBirthday = checkDateInput(userBirthday, ageMessage)

    const newUser = {
        _userFirstName: userFirstName.trim(),
        _userLastName: userLastName.trim(),
        birthday: userBirthday.trim(),
        getLogin() {
            return this._userFirstName[0].toLowerCase() + this._userLastName.toLowerCase()
        },
        getAge() {
            const dateParts = this.birthday.split('.')
            const convertedDate = [dateParts[2], (dateParts[1]), dateParts[0]].toString()
            const currentDate = new Date().getTime();
            return Math.floor((currentDate - (new Date(convertedDate).getTime())) / 31536000000);
        },
        getPassword() {
            return this._userFirstName[0].toUpperCase() + this._userLastName.toLowerCase() + this.birthday.slice(-4)
        },
        set userFirstName(value) {
            if (value !== '' && value !== null) {
                this._userFirstName = value
            }
        },
        set userLastName(value) {
            if (value !== '' && value !== null) {
                this._userLastName = value
            }
        },
        get userFirstName() {
            return this._userFirstName
        },
        get userLastName() {
            return this._userLastName
        },
        setFirstName(value) {
            if (value !== '' && value !== null) {
                this.userFirstName = value
            }
        },
        setLastName(value) {
            if (value !== '' && value !== null) {
                this.userLastName = value
            }
        },
        getUserFirstName() {
            return this.userFirstName
        },
        getUserLastName() {
            return this.userLastName
        }
    }
    return newUser
}


console.log(createNewUser().getAge())
console.log(createNewUser().getPassword())



