//1. setTimeout() calls a function one time after some time interval. Also you can use Set timeOut() with recursion if
// you want to call a function several times. setInterval() calls a function as much as you need with some interval.

//2. setTimeout() with 0 delay will call a function without delay for the first5 calls, after that the delay will be min
// 4 milliseconds

//3. ClearInterval function should be called to prevent further running of a function

const imgArray = Array.from(document.querySelectorAll('.image-to-show'))
const imgWrapper = document.querySelector('.images-wrapper')
const span = document.createElement('span')
const stopBtn = document.createElement('button')
const startBtn = document.createElement('button')

stopBtn.textContent = 'Stop'
startBtn.textContent = 'Start'

let message = 'Next image in '
let counter = 300
let start = Date.now() + 3000

span.style.cssText = 'display: block; height: 20px'

document.body.append(stopBtn)
document.body.append(startBtn)
document.body.prepend(span)
document.querySelectorAll('.image-to-show').forEach(element => element.remove())
span.textContent = `${message}` + ((start - Date.now()) / 1000)


function animateElement(element) {
    element.animate([{opacity: 0},
            {opacity: 1, offset: 0.5},
            {opacity: 0}],
        3000);
}

let i = 0

function startSlide() {
    imgArray[i].remove()
    i++
    if (i < imgArray.length) {
        imgWrapper.append(imgArray[i])
        animateElement(imgArray[i])

    } else {
        i = 0
        imgWrapper.append(imgArray[i])
        animateElement(imgArray[i])
    }
}

function startTimer() {
    counter--
    if (counter < 0) counter = 300
    span.textContent = `${message}` + ((start - Date.now()) / 1000)
}

function updateStartTime() {
    start = Date.now() + 3000
}

window.onload = () => {
    imgWrapper.append(imgArray[i])
    animateElement(imgArray[i])

    let sliderTimer = setInterval(startSlide, 3000)
    let timer = setInterval(startTimer, 10)
    let startTime = setInterval(updateStartTime, 3000)

    stopBtn.addEventListener('click', () => {
        clearInterval(sliderTimer)
        clearInterval(timer)
        clearInterval(startTime)
        span.textContent = ' '
        counter = 300
        start = Date.now() + 3000
        imgArray[i].getAnimations().forEach(animation => animation.finish())
    })

    startBtn.addEventListener('click', () => {
        animateElement(imgArray[i])
        clearInterval(sliderTimer)
        clearInterval(timer)
        clearInterval(startTime)
        counter = 300
        start = Date.now() + 3000
        sliderTimer = setInterval(startSlide, 3000)
        timer = setInterval(startTimer, 10)
        startTime = setInterval(updateStartTime, 3000)
    })
}