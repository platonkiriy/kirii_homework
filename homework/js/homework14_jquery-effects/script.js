const links = [...$('.menu-item-link')]

links.forEach(element => $(element).on('click', function (ev) {
    ev.preventDefault();
    $('html,body').animate({scrollTop: $(`#${element.dataset.link}`).offset().top})
}))

$(window).on('load', () => {
    $(window).on('scroll', () => {
        if (window.pageYOffset > window.innerHeight) {

            $('.slide-top').addClass('visible')
        } else $('.slide-top').removeClass('visible')
    })
})

$('.slide-top').on('click', ()=> $('html,body').animate({scrollTop: 0}))

$('.toggle-button').on('click', ()=> $('#posts').slideToggle())

//hopefully, I'll never use it again!