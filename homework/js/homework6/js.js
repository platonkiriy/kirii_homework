// forEch takes each element of the array and do some actions with the elements

const array = ['hello', 'world', 23, '23', null, undefined, '', [1, 2], true, false]

function filterBy(array, filterValue) {
    return  array.filter(element => typeof element !== filterValue)
}

console.log(filterBy(array, 'string'))


