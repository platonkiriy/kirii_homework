// Keyboard events should not be used for inputs because it won't work if some text is copy/pasted by a mouse clicks or
// from mobile device


const buttons = Array.from(document.querySelectorAll('.btn'))
window.addEventListener('keydown', event => {
    buttons.forEach(button => {
        button.style.backgroundColor = '#000000';
        if (button.innerText.toLowerCase() === event.key.toLowerCase()) {
            button.style.backgroundColor = 'blue';
        }
    })
})