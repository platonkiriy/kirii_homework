//1.
// var is a global variable and is visible overall in the code except the case when it's declared inside the function.
// Could be overwritten. Returns undefined when accessing a variable before it's declared

// const is a constant value and cannot be overwritten (if we are talking about primitive types) visibility of the const
// is limited by a block inside the brackets {}

// let is a variable whose visibility is limited by a block inside the brackets {}. Returns a ReferenceError when accessing a
// variable before it's declared

//2.
// Looks like var shouldn't be used because of its global visibility when you can't use the same variable name inside different
// blocks. Other than that if you reference to the var before it's declared it returns undefined instead of the ReferenceError

const ENTER_NAME_MESSAGE = 'Enter your name'
const ENTER_AGE_MESSAGE = 'Enter you age'
const NOT_ALLOWED_MESSAGE = 'You are not allowed to visit this website'
let userName = prompt(ENTER_NAME_MESSAGE)
let initialAge = prompt(ENTER_AGE_MESSAGE)
let parsedAge = parseInt(initialAge)

if (!isNaN(parsedAge) && parsedAge !== 0 && parsedAge >= 18 && parsedAge !== null && userName !== null && userName.trim() !== '') {
    if (parsedAge >= 18 && parsedAge <= 22) {
        let bool = confirm('Are you sure you want to continue?')
        if (bool === true) {
            alert(`Welcome ${userName}`)
        } else {
            alert(NOT_ALLOWED_MESSAGE)
        }
    } else {
        alert(`Welcome ${userName}`)
    }
} else {
    if (parsedAge < 18) {
        alert(NOT_ALLOWED_MESSAGE)
    } else {
        prompt(ENTER_NAME_MESSAGE, userName)
        prompt(ENTER_AGE_MESSAGE, initialAge)
    }
}