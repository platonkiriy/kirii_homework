// We use loops in programming in order to perform some actions repeatedly depending on condition true/false
// for example we can ask user to enter some data and repeat this action until user enters a valid data
// other than that we can fill some data into an object using a loop, this will take much less time and code

// let userNumber = parseInt(prompt('please enter a number'));
//
// while (isNaN(userNumber) || userNumber <= 0 || userNumber === null) {
//     userNumber = parseInt(prompt('please enter a positive number'));
// }
//
// if (userNumber >= 5) {
//     for (let i = 1; i <= userNumber; i++) {
//         if (i % 5 === 0) {
//             console.log(i)
//         }
//     }
// } else {
//     console.log('Sorry, no numbers')
// }

let m = parseInt(prompt('enter the first number'));
let n = parseInt(prompt('enter the second number'));
let primeNumbers = '';
while (m > n) {
  alert('first number should be less than the second one');
  m = parseInt(prompt('enter the first number'));
  n = parseInt(prompt('enter the second number'));
}

for (m; m <= n; m++) {
  let flag = 1;
  if (m > 2 && m % 2 !== 0) {
    for (let j = 3; j * j <= m; j = j + 2) {
      if (m % j === 0) {
        flag = 0;
        break;
      }
    }
  } else if (m !== 2) flag = 0;
  if (flag === 1) {
    primeNumbers += m + ', ';
  }
}

console.log(primeNumbers);
