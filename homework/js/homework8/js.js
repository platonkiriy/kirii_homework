// Event listener is a method that allows you to to listen events that are happening with elements like focus, click, etc,
// and depending on the event do some actions with the elements on a page

const inputField = document.querySelector('input')
const spanWrapper = document.createElement('div')
const textSpan = document.createElement('span')
const closeBtn = document.createElement('button')

spanWrapper.className = 'span-wrapper'
spanWrapper.style.cssText = 'position: absolute;' +
    'left: 65px;' +
    'top: 8px;' +
    'padding: 1px;' +
    'border: 1px solid black;' +
    'font-size: 10px;' +
    'border-radius: 10px;'

closeBtn.textContent = 'x'
closeBtn.style.cssText = 'border-radius: 50%; ' +
    'border: 1px solid black;' +
    'margin: 2px' +
    'padding: 1px;' +
    'background-color: white;' +
    'font-size: 10px;'


inputField.addEventListener('focus', () => {
    inputField.style.borderColor = 'green'
})

inputField.addEventListener('blur', () => {
    if (inputField.value > 0) {
        inputField.style.cssText = 'border-color: black; color: green;'
        textSpan.textContent = `Current price: ${inputField.value}`
        textSpan.style.cssText = 'margin: 2px;'
        document.querySelector('.container').prepend(spanWrapper)
        spanWrapper.prepend(textSpan)
        spanWrapper.append(closeBtn)
    } else if ( inputField.value < 0) {
        if (spanWrapper) spanWrapper.remove()
        textSpan.textContent = `Please enter correct price`
        textSpan.style.cssText = 'margin-left: 60px; font-size: 10px'
        inputField.style.cssText = 'border-color: red; color: black'
        document.querySelector('.container').append(textSpan)
    } else {
        if (spanWrapper) spanWrapper.remove()
        if (textSpan) textSpan.remove()
        inputField.style.cssText = 'border-color: black;'
    }
})

closeBtn.addEventListener('click', () => {
    spanWrapper.remove()
    inputField.value = '0'
    inputField.style.color = 'black'
})


