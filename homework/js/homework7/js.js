// DOM is a structured document represented as an object. Using DOM we can interact with its elements and modify page content

const array = ['hello', [1, 2, 3], 'world', 23, '23', , '', ['childItem1', 'childItem2'], true, false]

function createList(array, parent = document.body) {
    let ul = document.createElement('ul')
    parent.appendChild(ul)
    array.map(element => {
        let list = document.createElement('li')
        if (!Array.isArray(element)) {
            ul.appendChild(list)
            list.textContent = element
        } else {
            let childUl = document.createElement('ul')
            parent.lastChild.appendChild(childUl)
            element.map(element => {
                let childList = document.createElement('li')
                childUl.appendChild(childList)
                childList.textContent = element
            })
        }
    })
    let span = document.createElement('span')
    parent.prepend(span)
    window.onload = () => {
        let sec = 3
        span.textContent = 'Page will be cleared in ' + sec
        let timerId = setInterval(() => {
            sec--
            span.textContent = 'Page will be cleared in ' + sec
        }, 1000)
        setTimeout(() => {
            clearInterval(timerId)
            document.querySelector('body').textContent = ''
        }, 3000)
    }
}

createList(array)


